# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ:
```js
  const arr=[1,2,3];
  function fun(a,b){
    console.log(this.length+a+b)
  };

  const funArr=fun.bind(arr,3);// bind меняет контекст функции на объект передаваемый первым параметром, последующие параметры будут зарезервированными аргументами при вызове;
  funArr(2);        //8   аналогично fun.call(arr,3,2);
  fun.call(arr,3,2);  //8 call(this,[...arguments])Сразу вызывает функцию с контектом, передаваемым первым параметром, и аргументами после него. эквивалентно fun.bind(arr)(3,2);
  fun.call(arr,[2,3]);//8 Сразу вызывает функцию с контектом, передаваемым первым параметром, и массивом аргументов вторым
```
#### 2. Что такое стрелочная функция?
> Ответ: 2 в 1, синтаксический сахар и привязка лексического контекста.вот она a=>a-5
#### 3. Приведите свой пример конструктора. 
```js
// Ответ:
  function Lol(type){
    this.type=type;
    this.sey=function(){
      if(this.type!=='WoW fun'||this.type!=='korean starcrafter')
        console.log('lol');
      else
        console.log('kek');
    }
  }
  const kek=new Lol('korean starcrafter');
  kek.sey();// kek
```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
//bind
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(this), 1000)
    }
  }

  person.sayHello();

// arrow function
   const person1 = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(()=>{
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }
  person1.sayHello();

// apply,call
   const person2 = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }
  person2.sayHello.call(person2);
  person2.sayHello.apply(person2);
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
